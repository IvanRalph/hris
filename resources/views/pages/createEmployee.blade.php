@extends('adminlte::page')

@section('content_header')
<h1>@if(isset($showEmployee)) <a href='/employees' title='Go Back' class='unstyle'><i class='fa fa-arrow-left'></i></a> @endif {{ isset($showEmployee) ? ucfirst($showEmployee->firstname) . '\'s Employee Profile' : isset($updateEmployee) ? 'Update ' . ucfirst($updateEmployee->firstname) . '\'s Employee Profile' : 'Create Employee Profile' }}</h1>
@stop

@section('content')
<div class='notifications top-right'></div>
<form class="form-horizontal" action="{{ action('EmployeesController@store') }}" method="POST" id="createEmployeeForm">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="row">
		<div class="col-sm-6 form-group {{ $errors->has('id_number') ? 'has-error':'' }}">
			<label for="id_number" class="col-sm-3 control-label">ID Number</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="id_number" placeholder="Enter ID Number here" value="{{ old('id_number') ? old('id_number') : '' }}{{ isset($showEmployee->employee_id) ? $showEmployee->employee_id : '' }}" {{ isset($showEmployee->employee_id) ? 'readonly' : '' }}>
				@if($errors->has('id_number'))
				<span id="helpBlock2" class="help-block">{{ $errors->first('id_number') }}</span>
				@endif
			</div>
		</div>
		<div class="col-sm-6 form-group {{ $errors->has('date_hired') ? 'has-error':'' }}">
			<label for="date_hired" class="col-sm-3 control-label">Date Hired</label>
			<div class="col-sm-9">
				<input type="{{ isset($showEmployee) ? 'text' : 'date' }}" class="form-control" name="date_hired" value="{{ old('date_hired') ? old('date_hired') : '' }}{{ isset($showEmployee->date_hired) ? $showEmployee->date_hired : '' }}" {{ isset($showEmployee->date_hired) ? 'readonly' : '' }}>
				@if($errors->has('date_hired'))
				<span id="helpBlock2" class="help-block">{{ $errors->first('date_hired') }}</span>
				@endif
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 form-group {{ $errors->has('firstname') ? 'has-error':'' }}">
			<label for="firstname" class="col-sm-3 control-label">First Name</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="firstname" placeholder="Enter firstname here" value="{{ old('firstname') ? old('firstname') : '' }}{{ isset($showEmployee->firstname) ? $showEmployee->firstname : '' }}" {{ isset($showEmployee->firstname) ? 'readonly' : '' }}>
				@if($errors->has('firstname'))
				<span id="helpBlock2" class="help-block">{{ $errors->first('firstname') }}</span>
				@endif
			</div>
		</div>
		<div class="col-sm-6 form-group {{ $errors->has('department') ? 'has-error':'' }}">
			<label for="department" class="col-sm-3 control-label">Department</label>
			<div class="col-sm-9">
				@if(isset($showEmployee))
				<input type="text" class="form-control" name="firstname" placeholder="Enter department here" value="{{ isset($showEmployee->deptName) ? $showEmployee->deptName : '' }}" {{ isset($showEmployee->deptName) ? 'readonly' : '' }}>
				@else
				<select id="departmentName" class="form-control" name="department" {{ isset($showEmployee) ? 'readonly' : '' }}></select>
				@endif
				@if($errors->has('department'))
				<span id="helpBlock2" class="help-block">{{ $errors->first('department') }}</span>
				@endif
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 form-group {{ $errors->has('middlename') ? 'has-error':'' }}">
			<label for="middlename" class="col-sm-3 control-label">Middle Name</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="middlename" placeholder="Enter middlename here" value="{{ old('middlename') ? old('middlename') : '' }}{{ isset($showEmployee->middlename) ? $showEmployee->middlename : '' }}" {{ isset($showEmployee->middlename) ? 'readonly' : '' }}>
				@if($errors->has('middlename'))
				<span id="helpBlock2" class="help-block">{{ $errors->first('middlename') }}</span>
				@endif
			</div>
		</div>
		<div class="col-sm-6 form-group {{ $errors->has('position') ? 'has-error':'' }}">
			<label for="position" class="col-sm-3 control-label">Designation</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="position" placeholder="Enter designation here" value="{{ old('position') ? old('position') : '' }}{{ isset($showEmployee->position) ? $showEmployee->position : '' }}" {{ isset($showEmployee->position) ? 'readonly' : '' }}>
				@if($errors->has('position'))
				<span id="helpBlock2" class="help-block">{{ $errors->first('position') }}</span>
				@endif
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 form-group {{ $errors->has('lastname') ? 'has-error':'' }}">
			<label for="lastname" class="col-sm-3 control-label">Last Name</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="lastname" placeholder="Enter lastname here" value="{{ old('lastname') ? old('lastname') : '' }}{{ isset($showEmployee->lastname) ? $showEmployee->lastname : '' }}" {{ isset($showEmployee->lastname) ? 'readonly' : '' }}>
				@if($errors->has('lastname'))
				<span id="helpBlock2" class="help-block">{{ $errors->first('lastname') }}</span>
				@endif
			</div>
		</div>
		<div class="col-sm-6 form-group {{ $errors->has('employee_status') ? 'has-error':'' }}">
			<label for="employee_status" class="col-sm-3 control-label">Employee Status</label>
			<div class="col-sm-9">
				@if(isset($showEmployee))
				<input type="text" class="form-control" name="firstname" placeholder="Enter department here" value="{{ isset($showEmployee->deptName) ? $showEmployee->employee_status : '' }}" {{ isset($showEmployee->employee_status) ? 'readonly' : '' }}>
				@else
				<select class="form-control" name="employee_status">
					<option></option>
					<option value="Probationary">Probationary</option>
					<option value="Regular">Regular</option>
					<option value="Re-hire">Re-hire</option>
					<option value="Consultant">Consultant</option>
					<option value="Project Based">Project Based</option>
					<option value="Intern">Intern</option>
					<option value="Resigned">Resigned</option>
				</select>
				@endif
			</div>
			@if($errors->has('employee_status'))
			<span id="helpBlock2" class="help-block">{{ $errors->first('employee_status') }}</span>
			@endif
		</div>
	</div>
	<br>
	@if(!isset($showEmployee))
	<div class="row">
		<input type="submit" id="hiddenSubmit" value="submit" name="submit" hidden>
		<div class="col-sm-6 col-sm-offset-3">
			<div class="col-sm-4">
				<input type="button" name="save" id="submitBtn" value="Save" class="form-control btn btn-default">
			</div>

			<div class="col-sm-4">
				<input type="button" name="clear" id="clearBtn" value="Clear" class="form-control btn btn-default">
			</div>	

			<div class="col-sm-4">
				<input type="button" name="cancel" id="cancelBtn" value="Cancel" class="form-control btn btn-default">
			</div>
		</div>
	</div>
	@endif
</form>
@stop

@section('js')
@if(isset($employeeCreated))
<script>
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "Employee Profile has been created." }
		}).show();
	});
</script>
@endif

<script>
	@if(isset($showEmployee))
	$('#departmentName').val('{{ $showEmployee->deptName }}').trigger('change');
	$('select[name="employee_status"]').val('{{ $showEmployee->employee_status }}').trigger('change');
	@endif

	$(document).ready(function($) {
		$('select[name="employee_status"]').select2({
			placeholder: "Select employee status here",
			allowClear: true
		});

		$('#departmentName').select2({
			placeholder: "Select department here",
			allowClear: true,
			ajax: {
				url: '/autocomplete',
				dataType: 'json',
				data: function(params) {
					return {
						term: params.term
					}
				},
				processResults: function (data, page) {
					return {
						results: data
					};
				},
			}
		});
	})

	$("#submitBtn").on('click', function(e){
		e.preventDefault()
		swal({
			title: '',
			text: 'Are you sure you want to submit this request?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$('#hiddenSubmit').click();
		});
	})

	$('#clearBtn').on('click', function(e){
		e.preventDefault();
		swal({
			title: 'Are you sure you want to clear the form?',
			text: 'Doing so will reset the form',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$('#createEmployeeForm')[0].reset();
			$('select').val(null).trigger('change');
		});
	})

	$('#cancelBtn').on('click', function(e){
		e.preventDefault();
		swal({
			title: 'Are you sure you want to leave this page?',
			text: 'This will reset all your inputs',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			window.location.href = "/employees";
		});
	})
</script>
@stop