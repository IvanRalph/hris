@extends('adminlte::page')

@section('content_header')
<h1>Employees</h1>
@stop

@section('content')
<div class="row">
	<div class="col-sm-2">
		<select id="filterOption" class="form-control">
			<option value="all">All</option>
			<option value="full_name">Full Name</option>
			<option value="date_hired">Date Hired</option>
			<option value="department">Department</option>
			<option value="position">Position</option>
		</select>
	</div> 
	<div class="col-sm-2">
		<input type="text" name="search" id="search" class="form-control" placeholder="Enter keyword here">
	</div>
</div><br>
<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
	<thead>
		<tr>
			<th>id</th>
			<th>Full Name</th>
			<th>Date Hired</th>
			<th>Department</th>
			<th>Position</th>
			<th>Action</th>
		</tr>
	</thead>
</table>
@stop

@section('js')
<script>
	var table = $('#mainTable').DataTable({
		processing: true,
		'order': [],
		ajax: '/datatable/employeeData',
		columns: [
		{ data: 'id', visible: false },
		{ data: 'full_name', width: '30%' },
		{ data: 'date_hired', width: '20%' },
		{ data: 'department_id', width: '25%' },
		{ data: 'position', width: '25%' },
		{ data: null, width: '245px', searchable: false, sortable: false}
		],
		dom: 'r<"pull-right"B><"pull-left"l>tip',
		lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		pageLength: 10,
		columnDefs: [
		{
			targets: -1,
			render: function(a, b, data, d){
				var btn = "<a href='/employees/"+ data.id +"' class='btn btn-default'>View</a>";
				btn += "<a href='/employees/"+data.id+"/edit' class='btn btn-default'>Update</a>";
				btn += "<a href='' class='btn btn-default'>Deactivate</a>";

				return btn;
			}
		}
		],
		buttons: [
		{
			text: 'Import',
			action: function ( e, dt, node, config ) {
				window.location.href="/employees/create";
			}
		},
		{
			extend: 'excel',
			text: 'Export',
			exportOptions: {
				columns: [0,1,2,3,4,5,6,7,8,9,10]
			}
		},
		{
			text: 'Create Employee Profile',
			action: function ( e, dt, node, config ) {
				window.location.href="/employees/create";
			}
		}
		],
		"scrollX": true,
		"fixedHeader": true
	});

	$('#search').on( 'keyup', function () {
		var col;
		switch($('#filterOption').val()){
			case 'full_name':
				col = 1;
				break;
			case 'date_hired':
				col = 2;
				break;
			case 'department':
				col = 3;
				break;
			case 'position':
				col = 4;
				break;
			default:
				col = 5;
				break;
		}
	    if(col == 5){
	    	table
	    	    .search( this.value )
	    	    .draw();
	    }else{
	    	table
	    	    .columns( col )
	    	    .search( this.value )
	    	    .draw();
	    }
	} );
</script>
@stop