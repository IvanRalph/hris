<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::resource('employees', 'EmployeesController');

Route::resource('department', 'DepartmentController');

Route::get('/autocomplete', 'DepartmentController@autocomplete');

Route::get('/datatable/employeeData', 'DatatablesController@employeeData');
