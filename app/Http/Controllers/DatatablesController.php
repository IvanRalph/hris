<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use DB;
use DataTables;

class DatatablesController extends Controller
{
	public function employeeData(){
		$builder = Employee::query()->select('id', DB::raw('(SELECT CONCAT(firstname," ", lastname) FROM employee_details ed WHERE id = employee_details.id) AS full_name'), 'date_hired', 'department_id', 'position')
    	->orderBy('date_hired', 'DESC');
    	return datatables()->eloquent($builder)->toJson();
	}
}
