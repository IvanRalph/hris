<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Department;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.employees');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.createEmployee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_number'=>'required|numeric|unique:employee_details,employee_id',
            'firstname'=>'required',
            'middlename'=>'required',
            'lastname'=>'required',
            'date_hired'=>'required|date',
            'department'=>'required|exists:department_details,name',
            'position'=>'required',
            'employee_status'=>'required|in:Probationary,Regular,Re-hire,Consultant,Project Based,Intern,Resigned'
        ]);

        $deptId = Department::where('name', $request->input('department'))->first();

        $employee = new Employee;
        $employee->employee_id = $request->input('id_number');
        $employee->firstname = $request->input('firstname');
        $employee->middlename = $request->input('middlename');
        $employee->lastname = $request->input('lastname');
        $employee->date_hired = $request->input('date_hired');
        $employee->department_id = $deptId->department_id;
        $employee->position = $request->input('position');
        $employee->employee_status = $request->input('employee_status');
        $employee->save();

        return view('pages.createEmployee')->with('employeeCreated', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::where('id', $id)->first();
        $dept = Department::where('department_id', $employee->department_id)->first();
        $employee->deptName = $dept->name;
        return view('pages.createEmployee')->with('showEmployee', $employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::where('id', $id)->first();
        $dept = Department::where('department_id', $employee->department_id)->first();
        $employee->deptName = $dept->name;
        return view('pages.createEmployee')->with('updateEmployee', $employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
