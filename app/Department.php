<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'department_details';
    public $primaryKey = 'id';

    public $timestamps = false;
}
