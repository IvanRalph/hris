<?php

use Illuminate\Database\Seeder;

class DepartmentDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('department_details')->insert([
            'department_id'=>'ITD',
            'name'=>'Information Technology Department',
            'supervisor'=>1
        ]);
    }
}
