<?php

use Illuminate\Database\Seeder;

class EmployeeDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employee_details')->insert([
            'employee_id'=>'20170001',
            'firstname'=>'Juan',
            'lastname'=>'Dela Cruz',
            'department_id'=>1,
            'date_hired'=>'2017-01-01',
            'position' => 'Programmer',
            'employee_status'=>'Regular'
        ]);
    }
}
