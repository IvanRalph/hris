<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->string('firstname', 20);
            $table->string('middlename', 20)->nullable();
            $table->string('lastname', 20);
            $table->string('department_id');
            $table->date('date_hired');
            $table->string('position');
            $table->string('employee_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee_details');
    }
}
